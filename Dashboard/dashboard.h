#ifndef PANEL_H
#define PANEL_H

#include <QWidget>
#include <QTimeLine>
#include <QMutex>
#include <QTimer>
QT_BEGIN_NAMESPACE
namespace Ui { class Dashboard; }
QT_END_NAMESPACE

class Dashboard : public QWidget
{
    Q_OBJECT

public:
	Dashboard(QWidget *parent = nullptr);
    Dashboard(int minValue, int tickValue, int tick, int value, QString text, QWidget *parent = nullptr);
    ~Dashboard();

	void set(int minValue, int tickValue, int tick, int value, QString text);
public slots:
    void setValue(int value);
    void setText(QString text);
private slots:
    void timeOut();
protected:
    void paintEvent(QPaintEvent *event) override;
    void drawArrow(QPainter& painter);
private:
    double valueToAngle(int value);
private:
    Ui::Dashboard *ui;
	int minValue{ 0 };
	int tickValue{ 10 };
	int tick{ 10 };
	int value{ 0 };
	QString text{ "panel" };

	double frameValue{ 0 };
	double arrowAngle{ 120 };
	double targetAngle{ 120 };
    QTimer timer;
	int index{ 0 };
};
#endif // PANEL_H
