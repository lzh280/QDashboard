#include "window.h"
#include "ui_window.h"
#include "dashboard.h"

Window::Window(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Window)
{
    ui->setupUi(this);
    Dashboard *w = new Dashboard(-50,10,10,-31,"hello");
    ui->widget->layout()->addWidget(w);
    ui->spinBox->setRange(-50,50);
    ui->spinBox->setValue(-31);
    connect(ui->spinBox, SIGNAL(valueChanged(int)), w, SLOT(setValue(int)));
    connect(ui->horizontalSlider, SIGNAL(sliderMoved(int)), w, SLOT(setValue(int)));
}

Window::~Window()
{
    delete ui;
}
